# plays-statement-perl

Simple perl example for refactoring workshop taken from M.Fowler's book second edition.


## Run tests

```
prove t/statement.t
```

## Run coverage

```
perl -MDevel::Cover t/statement.t
```


## Docker

### Build image
```
docker build -t plays-perl .
```

### run interactive shell
```
docker run -v "$(pwd):/usr/src/app" -it plays-perl /bin/bash
```

