#!/usr/bin/perl

use strict;
use warnings;
use POSIX qw/fmax floor/;
use File::Slurp;
use JSON::Parse 'parse_json';
use Number::Format;

my $plays_content = read_file('plays.json');
my $all_plays = parse_json($plays_content);
my $invoice_content = read_file('invoice.json');
my $current_invoice = parse_json($invoice_content);

print statement($current_invoice, $all_plays);

sub statement {
    my ($invoice, $plays) = @_;
    my $totalAmount = 0;
    my $volumeCredits = 0;
    my $result = "Statement for $invoice->{customer}\n";
    my $format = Number::Format->new(-int_curr_symbol => '$');
    for my $perf (@{$invoice->{performances}}) {
        my $play = $plays->{$perf->{playID}};
        my $thisAmount = 0;

        if ($play->{type} eq 'tragedy') {
            $thisAmount = 40000;
            if ($perf->{audience} > 30) {
                $thisAmount += 1000 * ($perf->{audience} - 30);
            }
        }
        elsif ($play->{type} eq 'comedy') {
            $thisAmount = 30000;
            if ($perf->{audience} > 20) {
                $thisAmount += 10000 + 500 * ($perf->{audience} - 20);
            }
            $thisAmount += 300 * $perf->{audience};
        }
        else {
            die("unknown type: $play->{type}")
        }
        $volumeCredits += fmax($perf->{audience} - 30, 0);
        # add extra credit for every ten comedy attendees
        $volumeCredits += floor($perf->{audience} / 5)
            if $play->{type} eq "comedy";
        # print line for this order
        $totalAmount += $thisAmount;
        $result .= "  $play->{name}: " . $format->format_price($thisAmount / 100). " ($perf->{audience} seats)\n";
    }
    $result .= "Amount owed is " . $format->format_price($totalAmount / 100) . "\n";
    $result .= "You earned ${volumeCredits} credits\n";
    return $result;
}