use strict;
use warnings;

use Test::Spec;
use Test::Exception;

require("./statement.pl");

describe "report statement should" => sub {

    my $invoice = {
        "customer"     => "BigCo",
        "performances" => [
            {
                "playID"   => "hamlet",
                "audience" => 55
            },
            {
                "playID"   => "as-like",
                "audience" => 35
            },
            {
                "playID"   => "othello",
                "audience" => 40
            }
        ]
    };

    it "be correct" => sub {

        my $plays = {
            "hamlet"  => { "name" => "Hamlet", "type" => "tragedy" },
            "as-like" => { "name" => "As You Like It", "type" => "comedy" },
            "othello" => { "name" => "Othello", "type" => "tragedy" }
        };

        my $expected = "Statement for BigCo\n" .
            "  Hamlet: \$ 650.00 (55 seats)\n" .
            "  As You Like It: \$ 580.00 (35 seats)\n" .
            "  Othello: \$ 500.00 (40 seats)\n" .
            "Amount owed is \$ 1,730.00\n" .
            "You earned 47 credits\n";

        is(statement($invoice, $plays), $expected)
    };

    it "throw error on unknown play type" => sub {
        my $plays = {
            "hamlet" => { "name" => "Hamlet", "type" => "unknown" },
        };

        dies_ok { statement($invoice, $plays) }

    };
};

runtests;

